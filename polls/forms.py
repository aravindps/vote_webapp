from django import forms
from .models import Choice


class NewPollForm(forms.Form):

    question_text = forms.CharField(label='Question', max_length=100, initial='Enter your question')
    choice_a = forms.CharField(label='A', max_length=100)
    choice_b = forms.CharField(label='B', max_length=100)
    choice_c = forms.CharField(label='C', max_length=100)
    choice_d = forms.CharField(label='D', max_length=100)
    active = forms.BooleanField(label='Activate', initial=True, help_text="Check to make activate the poll url")


class VotePollForm(forms.Form):

    choices = None

    def __init__(self, question_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = [ (o.id, str(o)) for o in Choice.objects.filter(question__pk=question_id)]
        print("Setting form value", choices)
        self.fields['choice'] = forms.ChoiceField(label='Options',
                                                  choices=choices, widget=forms.RadioSelect)


