from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render

from .forms import NewPollForm, VotePollForm
from .models import Choice, Question


def index(request):

    questions = Question.objects.filter(is_active=1)
    question_list = []
    choice_list = []
    choice_max_count = 0
    for question in questions:
        choices = Choice.objects.filter(question_id=question.id)
        choice_count = 0
        for choice in choices:
            choice_count += 1
            choice_list.append(choice)

        if choice_max_count < choice_count:
            choice_max_count = choice_count

        question_list.append(question)

    return render(request, 'polls/poll_list.html',  {"questions" : question_list, "choices" : choice_list, "choice_max_count": range(0, choice_max_count)})


def logout_view(request):

    logout(request)
    return redirect('/')


@login_required(login_url="/admin/login")
def poll_view(request, pk):

    print("Poll key", pk)
    if pk and Question.objects.filter(id=pk):

        # get question
        question = Question.objects.filter(id=pk)[0]
        print("Question: ", question)

        if question.is_active:
            # if this is a POST request we need to process the form data

            if request.method == 'POST':

                print("Got POST request")
                form = VotePollForm(pk,request.POST)
                # check whether it's valid:
                if form.is_valid():
                    data = form.cleaned_data
                    try:
                        # Getting question text
                        choice_id = data['choice']
                        choice = Choice.objects.filter(id=int(choice_id))[0];
                        print("Selected option[" + choice_id + "]: " + choice.choice_text)
                        choice.votes += 1
                        choice.save()

                    except IntegrityError:
                        return render(request, 'polls/message.html', {'error': True, 'message': "The question already exists. Please try with a different question."})
                    except:
                        return render(request, 'polls/message.html', {'error': True, 'message': "Failed to save the post. Please contact support for more details"})

                    return render(request, 'polls/message.html', {'error': False, 'message': "Your vote is recorded"})

            # if a GET (or any other method) we'll create a blank form
            else:
                # create a form instance and populate it with data from the request:
                form = VotePollForm(pk)

            return render(request, 'polls/poll_vote.html', {'form': form, 'question': question})

        else: # Poll is inactive
            return HttpResponse("<h4 align='center'>Post is no more available</h4>")
    else: # invalid poll number
        return HttpResponse("<h4 align='center'>Please verify the Poll Number</h4>")


@login_required(login_url="/admin/login")
def poll_new(request):

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NewPollForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            data = form.cleaned_data

            try:
                # Getting question text
                question_text = data['question_text']
                question = Question()
                question.question_text = question_text
                question.is_active = data['active']
                print("Saving question ", question)
                question.save()

                choices_value = [data['choice_a'], data['choice_b'], data['choice_c'], data['choice_d']]
                choices = check_and_create_choices(choices_value, question)
                if choices:
                    save_choices(choices)

            except IntegrityError:
                return render(request, 'polls/message.html', {'error': True, 'message': "The question already exists. Please try with a different question."})
            except:
                return render(request, 'polls/message.html', {'error': True, 'message': "Failed to save the post. Please contact support for more details"})

            return render(request, 'polls/message.html', {'error': False, 'message': "You can share the <a href=/polls/" + str(question.id) + ">url</a> question with your friends."})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NewPollForm()

    return render(request, 'polls/new_polls.html', {'form': form})


def save_choices(listChoices):

    for choice in listChoices:

        print("Saving choice", choice)
        choice.save()


def check_and_create_choices(listChoices, question):

    choices = []
    for choice_value in listChoices:
        if choice_value and len(choice_value) == 0:
            return None
        choice = Choice()
        choice.choice_text = choice_value
        choice.question = question
        choices.append(choice)

    return choices
