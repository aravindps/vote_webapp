from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<pk>\d+)$', views.poll_view, name="polls/poll_list.html"),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^new/?$', views.poll_new, name='post_new'),
]
