Vote App

## Requirement

* Python 3.5.1
* Django 1.9.4 - ``sudo pip install Django==1.9.4``
* MySql application - ``sudo apt-get install mysql-server mysql-client mysql-common``
* mysqlclient 1.9.4 - ``sudo pip install mysqlclient``

## Deploy server:

- Switch to project directory - ``cd project/directory``
- Configure mysql with username and password in project/directory/vote/settings.py
![db_settings.png](https://bitbucket.org/repo/ed9A8p/images/402238278-db_settings.png)# 
- Login to mysql and create vote database - ``CREATE DATABASE vote DEFAULT CHARACTER SET UTF8;``
- Create schema files - ``python manage.py makemigrations``
- Create tables - ``python manage.py migrate``
- Create admin user - ``python manage.py createsuperuser``
- Run the application ``python manage.py runserver``